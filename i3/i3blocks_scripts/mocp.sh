#!/bin/sh
# Get song title from mocp if player not paused
# This is just to save space on the bar when the info is not needed

state=$(mocp -i  | grep -E '^State' | cut -c 8- )
output=$(mocp -i  \
	| grep -E '^Title' \
	| iconv -f utf-8 -t ascii//translit \
	| cut -c 7-35)

if [ "$state" != 'PAUSE' ]
	then
		echo "$output"
fi
