"" Encoding
set encoding=utf-8
set fileencoding=utf-8
set fileencodings=utf-8
set ttyfast

"" Fix backspace indent
set backspace=indent,eol,start

" Tabs options
set tabstop=4
set shiftwidth=4
set softtabstop=4
set noexpandtab

" Visuals
set t_Co=256
set background=dark
set cursorline
set cursorcolumn
let &colorcolumn=join(range(81,999),",")		" highlight column 80
let &colorcolumn="80,".join(range(120,999),",") " highlight column 120 onward
set ruler										" show the cursor position all the time
set showmatch									" highlight matching braces
set showmode									" show insert/replace/visual mode
set noshowmode									" Hide default --INSERT--
set laststatus=2								" always show status bar
set showcmd										" shows cmd in the bar
set list listchars=tab:>-,trail:.,extends:>,precedes:<,nbsp:+ " Display spaces and tabs

set undofile									" creates Undo file to allow undo after closing a file
set nocompatible								" avoid vi compatibility
set modeline
set modelines=10								" to prevent file exploits
set updatetime=100								" update time (usefull for git diff)
set nowrap										" don't wrap around long lines
set shortmess=a									" avoid 'press enter or...' message on startup
let mapleader = ","								" Leader
let no_buffers_menu=1

" Enable syntax highlighting
syntax enable
syntax on
let python_highlight_all=1

" Line Numbers
set number
set relativenumber
set numberwidth
set lines

" Searching
set ignorecase " Ignore case when searching
set smartcase " When searching try to be smart about cases
set hlsearch " Highlight search results
set incsearch " Makes search act like search in modern browsers

" Search mappings: These will make it so that going to the next one in a
" search will center on the line it's found in.
nnoremap n nzzzv
nnoremap N Nzzzv

" Move to brackets with tabs
nnoremap <tab> %
vnoremap <tab> %

" Move by file line and not screen line
nnoremap j gj
nnoremap k gk

set pastetoggle=<F2> " toggle paste mode on/off

" File cleanup
nnoremap <F5> :%s/\s\+$//e<CR>

" Rope mapping to jump to definition

" map <leader>j :RopeGotoDefinition
" map <leader>r :RopeRename

set statusline=%F%m%r%h%w%=(%{&ff}/%Y)\ (line\ %l\/%L,\ col\ %c)\
if exists("*fugitive#statusline")
	set statusline+=%{fugitive#statusline()}
endif

" Remaps search keys all defaulting to Perl compatible regex
"map <space> /\v
nnoremap / /\v
vnoremap / /\v
map <c-space> ?
nnoremap <leader><space> :noh<cr>

" Unmap arrows
nnoremap <Up> :echomsg "use k"<cr>
nnoremap <Down> :echomsg "use j"<cr>
nnoremap <Left> :echomsg "use h"<cr>
nnoremap <Right> :echomsg "use l"<cr>

" Get out of any mode with jk instead of ESC
imap jk <Esc>

" vv to generate new vertical split
nnoremap <silent> vv <C-w>v
nnoremap <silent> vs <C-w>s

"split navigations
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

" fix smartindent issue of unindenting comments
filetype indent on
filetype on " enable automatic file detection

" Allow syntax highlighting in markdown fenced languages
let g:markdown_fenced_languages = ['html', 'python', 'bash=sh']

" vim-airline
let g:airline_theme = 'powerlineish'
let g:airline#extensions#branch#enabled = 1
let g:airline#extensions#ale#enabled = 1
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tagbar#enabled = 1
let g:airline_skip_empty_sections = 1

if !exists('g:airline_symbols')
  let g:airline_symbols = {}
endif

if !exists('g:airline_powerline_fonts')
  let g:airline#extensions#tabline#left_sep = ' '
  let g:airline#extensions#tabline#left_alt_sep = '|'
  let g:airline_left_sep          = '▶'
  let g:airline_left_alt_sep      = '»'
  let g:airline_right_sep         = '◀'
  let g:airline_right_alt_sep     = '«'
  let g:airline#extensions#branch#prefix     = '⤴' "➔, ➥, ⎇
  let g:airline#extensions#readonly#symbol   = '⊘'
  let g:airline#extensions#linecolumn#prefix = '¶'
  let g:airline#extensions#paste#symbol      = 'ρ'
  let g:airline_symbols.linenr    = '␊'
  let g:airline_symbols.branch    = '⎇'
  let g:airline_symbols.paste     = 'ρ'
  let g:airline_symbols.paste     = 'Þ'
  let g:airline_symbols.paste     = '∥'
  let g:airline_symbols.whitespace = 'Ξ'
else
  let g:airline#extensions#tabline#left_sep = ''
  let g:airline#extensions#tabline#left_alt_sep = ''
endif

"" NERDTree configuration
let g:NERDTreeChDirMode=2
let g:NERDTreeIgnore=['\.rbc$', '\~$', '\.pyc$', '\.db$', '\.sqlite$', '__pycache__']
let g:NERDTreeSortOrder=['^__\.py$', '\/$', '*', '\.swp$', '\.bak$', '\~$']
let g:NERDTreeShowBookmarks=1
let g:nerdtree_tabs_focus_on_files=1
let g:NERDTreeWinSize = 30
set wildignore+=*/tmp/*,*.so,*.swp,*.zip,*.pyc,*.db,*.sqlite
nnoremap <Leader>t :NERDTreeToggle<CR>
" nnoremap <leader>f :NERDTreeFind<CR>

" terminal emulation
nnoremap <silent> <leader>sh :terminal<CR>

" ale
let g:ale_linters = {}

" Tab options for PHP
augroup vimrc-php
	autocmd!
	autocmd Filetype php setlocal ts=4 sw=4
augroup END

" javascript
let g:javascript_enable_domhtmlcss = 1

" vim-javascript
augroup vimrc-javascript
	autocmd!
	autocmd FileType javascript setl ts=2|setl sw=2|setl expandtab softtabstop=2
augroup END

" python
" vim-python
augroup vimrc-python
	autocmd!
	autocmd FileType python setlocal expandtab sw=4 ts=8
		\ colorcolumn=79 formatoptions+=croq softtabstop=4
		\ cinwords=if,elif,else,for,while,try,except,finally,def,class,with
augroup END

" jedi-vim
let g:jedi#popup_on_dot = 0
let g:jedi#goto_assignments_command = "<leader>g"
let g:jedi#goto_definitions_command = "<leader>d"
let g:jedi#documentation_command = "K"
let g:jedi#usages_command = "<leader>n"
let g:jedi#rename_command = "<leader>r"
let g:jedi#show_call_signatures = "0"
let g:jedi#completions_command = "<C-Space>"
let g:jedi#smart_auto_mappings = 0

" ale
:call extend(g:ale_linters, {
    \'python': ['flake8'], })

" vim-airline
let g:airline#extensions#virtualenv#enabled = 1

" Syntax highlight
" Default highlight is better than polyglot
let g:polyglot_disabled = ['python']
let python_highlight_all = 1

"" The PC is fast enough, do syntax highlight syncing from start unless 200 lines
augroup vimrc-sync-fromstart
  autocmd!
  autocmd BufEnter * :syntax sync maxlines=200
augroup END

"" Remember cursor position
augroup vimrc-remember-cursor-position
  autocmd!
  autocmd BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g`\"" | endif
augroup END

set autoread

" Tagbar
nmap <silent> <F4> :TagbarToggle<CR>
let g:tagbar_autofocus = 1

" Gutentags specifics
" How Gutentags should look for projects
let g:gutentags_add_default_project_roots = 0
let g:gutentags_project_root = ['package.json', '.git']

" When to generate new tags
let g:gutentags_generate_on_new = 1
let g:gutentags_generate_on_missing = 1
let g:gutentags_generate_on_write = 1
let g:gutentags_generate_on_empty_buffer = 0

" Save tags in a specific dir instead of in repo
let g:gutentags_cache_dir = expand('~/.cache/vim/ctags/')

" Save more info on the tags
"a: Access (or export) of class members i: Inheritance information
"l: Language of input file containing tag m: Implementation information
"n: Line number of tag definition S: Signature of routine (e.g. prototype or parameter list)
let g:gutentags_ctags_extra_args = [
      \ '--tag-relative=yes',
      \ '--fields=+ailmnS',
      \ ]

" Files it should ignore when creating tags
let g:gutentags_ctags_exclude = [
      \ '*.git', '*.svg', '*.hg',
      \ '*/tests/*',
      \ 'build',
      \ 'dist',
      \ '*sites/*/files/*',
      \ 'bin',
      \ 'node_modules',
      \ 'bower_components',
      \ 'cache',
      \ 'compiled',
      \ 'docs',
      \ 'example',
      \ 'bundle',
      \ 'vendor',
      \ '*.md',
      \ '*-lock.json',
      \ '*.lock',
      \ '*bundle*.js',
      \ '*build*.js',
      \ '.*rc*',
      \ '*.min.*',
      \ '*.map',
      \ '*.bak',
      \ '*.zip',
      \ '*.pyc',
      \ '*.class',
      \ '*.sln',
      \ '*.Master',
      \ '*.csproj',
      \ '*.tmp',
      \ '*.csproj.user',
      \ '*.cache',
      \ '*.pdb',
      \ 'tags*',
      \ 'cscope.*',
      \ '*.css',
      \ '*.less',
      \ '*.scss',
      \ '*.exe', '*.dll',
      \ '*.mp3', '*.ogg', '*.flac',
      \ '*.swp', '*.swo',
      \ '*.bmp', '*.gif', '*.ico', '*.jpg', '*.png',
      \ '*.rar', '*.zip', '*.tar', '*.tar.gz', '*.tar.xz', '*.tar.bz2',
      \ '*.pdf', '*.doc', '*.docx', '*.ppt', '*.pptx',
      \ ]

" PHP CodeSniffer
" Test current file syntax
map <F3> :! phpcs --standard=MediaWiki % <enter>

" Rainbow parentheses
let g:rainbow_active = 1

" Start of Vim-Plug manager
call plug#begin()

" Themes and Colors
Plug 'ajh17/spacegray.vim'
Plug 'jaredgorski/SpaceCamp/'
Plug 'cocopon/iceberg.vim'
Plug 'luochen1990/rainbow'
Plug 'amadeus/vim-evokai'
Plug 'vim-scripts/Zenburn'
Plug 'jonathanfilip/vim-lucius'
Plug 'tomasr/molokai'
Plug 'dracula/vim', { 'as': 'dracula' }
Plug 'mhinz/vim-janah'
Plug 'joshdick/onedark.vim'

" PHP specific
Plug 'StanAngeloff/php.vim'
" Plug '2072/vim-syntax-for-PHP'
Plug '2072/PHP-Indenting-for-VIm'
Plug 'shawncplus/phpcomplete.vim'
" Plug 'lvht/phpcd.vim'

" Python specific
Plug 'maralla/completor.vim'
" Plug 'vim-python/python-syntax'
Plug 'davidhalter/jedi-vim'
Plug 'python-mode/python-mode', { 'for': 'python', 'branch': 'develop' }
Plug 'Vimjas/vim-python-pep8-indent'

" Diverse syntax highlighting
Plug 'sheerun/vim-polyglot'
"Plug 'elzr/vim-json'
"Plug 'pangloss/vim-javascript'
"Plug 'mboughaba/i3config.vim'
"Plug 'chr4/nginx.vim'
"Plug 'ericpruitt/tmux.vim', {'rtp': 'vim/'}
"Plug 'vim-perl/vim-perl', { 'for': 'perl' }

" Git specific
Plug 'tpope/vim-fugitive'
Plug 'airblade/vim-gitgutter'
Plug 'itchyny/vim-gitbranch'

" Linting
Plug 'w0rp/ale'

" Generic
Plug 'majutsushi/tagbar' " browse tree of tags of current file
Plug 'tpope/vim-commentary' " comment stuff easily
Plug 'tpope/vim-surround'
Plug 'scrooloose/nerdtree'
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'
Plug 'ludovicchabant/vim-gutentags'
Plug 'itchyny/lightline.vim'
Plug 'vim-airline/vim-airline/'
Plug 'vim-airline/vim-airline-themes'
Plug 'ctrlpvim/ctrlp.vim'
Plug 'christoomey/vim-tmux-navigator'
Plug 'tpope/vim-obsession' " Betteer session
Plug 'mhinz/vim-startify' " Fancy start menu for vim

call plug#end()

colorscheme onedark " default coloscheme at the botton to ensure it loads
